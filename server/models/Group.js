var mongoose = require('mongoose');

var Group = new mongoose.Schema({
  name: String,
  about: String,
});

module.exports = mongoose.model('Group', Group);
