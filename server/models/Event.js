var mongoose = require('mongoose');

var Event = new mongoose.Schema({
  name: String,
  date: Date,
  info: String,
  learn: [],
  active: Boolean,
  group: { type: mongoose.Types.ObjectId, ref: 'Group' },
});

module.exports = mongoose.model('Event', Event);
