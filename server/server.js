const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const passport = require('passport');
const nodemailer = require('nodemailer');
var mongoose = require('mongoose');
mongoose.connect('mongodb://taha:taha123lol@ds259738.mlab.com:59738/morrison');

var Event = require('./models/Event.js');
var Group = require('./models/Group.js');

var Trello = require('trello-node-api')(
  '66c25f1d7195ee766371734b6aa154f3',
  '9e7225f9fe852f85f70b7f4dc7a707c3a4408afe4e3502b280787dc6c96fe51d',
);

// BEsbYKv5
// 5e25a7988a21cf668c00de89

// var data = {
//   name: 'Задача',
//   desc: 'Корочеы',
//   pos: 'top',
//   idList: '5e25a7988a21cf668c00de89', //REQUIRED
// };

// Trello.card
//   .create(data)
//   .then(function(response) {
//     console.log('response ', response);
//   })
//   .catch(function(error) {
//     console.log('error', error);
//   });

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'ayazhan.tugelbayeva@gmail.com',
    pass: 'tuinpylstgcpfllj',
  },
});

const app = express();
const port = process.env.PORT || 3001;

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(passport.initialize());

// require('./config/passport')(passport);

app.use(express.static('client/public/'));
app.use('/images/', express.static('../client/images'));
app.use('/fonts/', express.static('../client/style/fonts'));

app.post('/sendmail/', function(req, res) {
  console.log(req.body.mail);

  const msg = {
    to: 'takhirmunarbekov@gmail.com',
    from: req.body.mail,
    subject: req.body.name + ' (Проект с сайта)',
    text: req.body.msg + ' | ' + req.body.mail,
  };

  transporter.sendMail(msg, function(error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });

  res.send(true);
});

app.post('/api/event', function(req, res, next) {
  let post = new Event({
    name: req.body.name,
    date: req.body.date,
    info: req.body.info,
    learn: req.body.learn,
    group: req.body.group._id,
    active: true,
  });

  post.save(function(err) {
    if (err) return res.status(400).end();
    res.send(post);
  });
});

app.post('/api/send/', function(req, res, next) {
  var data = {
    name: 'Заявка ' + req.body.name + ' || ' + req.body.phone,
    desc:
      'Заявка с code.morrison.kz || ' +
      req.body.name +
      ' || ' +
      req.body.phone +
      ' || ' +
      req.body.email,
    pos: 'top',
    idList: '5e25a7988a21cf668c00de89', //REQUIRED
  };

  Trello.card
    .create(data)
    .then(function(response) {
      console.log('response ', response);
    })
    .catch(function(error) {
      console.log('error', error);
    });
  res.send('');
});

app.post('/api/group', function(req, res, next) {
  let post = new Group({
    name: req.body.name,
    about: '',
  });

  post.save(function(err) {
    if (err) return res.status(400).end();
    res.send(post);
  });
});

app.get('/api/events', function(req, res, next) {
  Event.find({}).exec(function(err, posts) {
    if (err) return res.status(400).end();
    res.send(posts);
  });
});

app.get('/api/groups', function(req, res, next) {
  Group.find({}).exec(function(err, posts) {
    if (err) return res.status(400).end();
    res.send(posts);
  });
});

app.get('/api/event', function(req, res, next) {
  Event.findOne({ id: req.body.id }).exec(function(err, post) {
    if (err) return res.status(400).end();
    res.send(post);
  });
});

app.get('*', (req, res) => {
  res.sendFile('./index.html', {
    root: path.join(__dirname, '../client/public/'),
  });
});

app.listen(port, () => {
  console.log(`SERVER RUNNNING ON PORT ${port}`);
});

module.exports = app;
