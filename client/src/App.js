import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, HashRouter } from 'react-router-dom';
import ReactGA from 'react-ga';

import Routes from './routes';
import ScrollToTop from './utlis/scrollTop';

// import { getProfile } from "./actions/profileActions";

ReactGA.initialize('UA-144467595-1');
ReactGA.pageview('/');

// store.dispatch(getMovieData());

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <ScrollToTop>
          <Routes />
        </ScrollToTop>
      </BrowserRouter>
    );
  }
}

export default App;
