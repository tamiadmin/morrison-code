import React, { Fragment, Component } from 'react';

import './event.scss';
import axios from 'axios';
import Form from '../../components/form';

class EventsItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event: null,
      mounth: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь',
      ],
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {};
  }

  componentDidMount() {
    axios
      .get(`/api/event`, {
        id: this.props.match.params.id,
      })
      .then(response => {
        this.setState({
          event: response.data,
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidUpdate(prevProps) {}

  render() {
    return !this.state.event ? null : (
      <Fragment>
        <div className='event'>
          <div className='event-top'>
            <div className='container'>
              <div className='event-top-inner'>
                <div className='event-top-content'>
                  <div className='event-top-title'>{this.state.event.name}</div>
                  <div className='event-top-date'>
                    {this.state.event.date.substring(8, 10)}{' '}
                    {
                      this.state.mounth[
                        parseInt(this.state.event.date.substring(5, 7)) - 1
                      ]
                    }{' '}
                    {this.state.event.date.substring(0, 4)}
                  </div>
                  <div className='event-top-text'>{this.state.event.info}</div>
                  <div className='event-top-learn'>
                    <div className='event-top-learn-title'>
                      Чему вы научитесь
                    </div>
                    {this.state.event.learn.map((i, k) => {
                      return (
                        <li key={k} className='event-top-learn-i'>
                          {i}
                        </li>
                      );
                    })}
                  </div>
                </div>
                <div className='event-form'>
                  <div className='event-form-title'>Записаться на урок</div>
                  <Form />
                </div>
              </div>
            </div>
          </div>
          {/* <div className='event-all'>
            <div className='container'>
              <div className='event-all-inner'>Все события</div>
            </div>
          </div>
          <div className='event-items'>
            <div className='container'>
              <div className='event-items-inner'>
                {[...Array(6)].map((i, k) => {
                  return (
                    <div key={k} className='event-items-i'>
                      <div className='event-items-i-type'>
                        <div className='event-items-i-badge'>
                          <span>{this.state.activeEvent}</span>
                        </div>
                        <div className='event-items-i-name'>
                          Вопрос - ответ. Все для веб дизайнера
                        </div>
                      </div>
                      <div className='event-items-i-start'>
                        15 декабря 2017 в 16:00
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div> */}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default (EventsItem);
