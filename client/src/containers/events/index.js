import React, { Fragment, Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import './events.scss';

class Events extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeEvent: {
        _id: '',
        name: '',
      },
      events: [],
      groups: [],
      mounth: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь',
      ],
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {};
  }

  componentDidMount() {
    axios
      .get(`/api/events`)
      .then(response => {
        this.setState({
          events: response.data,
        });
      })
      .catch(err => {});

    axios
      .get(`/api/groups`)
      .then(response => {
        this.setState({
          groups: response.data,
          activeEvent: response.data[1],
        });
      })
      .catch(err => {});
  }

  componentDidUpdate(prevProps) {}

  render() {
    return (
      <Fragment>
        <div className='events'>
          <div className='events-top'>
            <div className='container'>
              <div className='events-top-inner'>
                <div className='events-top-content'>
                  <div className='events-top-title'>СОБЫТИЯ</div>
                  <div className='events-top-text'>
                    Здесь вы можете записать на бесплатные уроки. Которые
                    помогут вам понять и изучить основы самых лучших тем и
                    технологий в IT. И ознакомиться с нашими курсами,
                    преподавателями и убедиться в том что мы то что вам надо.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='events-type'>
            <div className='container'>
              <div className='events-type-inner'>
                {this.state.groups.map((i, k) => {
                  return this.state.activeEvent.name === i.name ? (
                    <span className='events-type-inner-active'>{i.name}</span>
                  ) : (
                    <span
                      onClick={() =>
                        this.setState({
                          activeEvent: i,
                        })
                      }
                    >
                      {i.name}
                    </span>
                  );
                })}
              </div>
            </div>
          </div>
          <div className='events-items'>
            <div className='container'>
              <div className='events-items-inner'>
                {this.state.events.map((i, k) => {
                  return this.state.activeEvent._id === i.group ? null : (
                    <Link
                      to={'/events/' + i._id}
                      key={k}
                      className='events-items-i'
                    >
                      <div className='events-items-i-type'>
                        <div className='events-items-i-badge'>
                          {this.state.activeEvent ? (
                            <span>{this.state.activeEvent.name}</span>
                          ) : null}
                        </div>
                        <div className='events-items-i-name'>{i.name}</div>
                      </div>
                      <div className='events-items-i-start'>
                        {i.date.substring(8, 10)}{' '}
                        {
                          this.state.mounth[
                            parseInt(i.date.substring(5, 7)) - 1
                          ]
                        }{' '}
                        {i.date.substring(0, 4)}
                      </div>
                    </Link>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default Events;
