import React, { Fragment, Component } from 'react';


import './admin.scss';
import axios from 'axios';

const EventModal = props =>
  props.event ? (
    <div className='admin-modal'>
      <div className='admin-modal-bg' onClick={() => props.close()}></div>
      <div className='admin-modal-inner'>
        <div className='admin-modal-content'>
          <div className='admin-modal-title'>Событие</div>
          <div className='admin-modal-text'>
            <div className='admin-modal-text-badge'>Навание:</div>
            <div className='admin-modal-text-inner'>{props.event.name}</div>
          </div>
          <div className='admin-modal-text'>
            <div className='admin-modal-text-badge'>Время:</div>
            <div className='admin-modal-text-inner'>{props.event.date}</div>
          </div>
          <div className='admin-modal-text'>
            <div className='admin-modal-text-badge'>Описание:</div>
            <div className='admin-modal-text-inner'>{props.event.info}</div>
          </div>
          <div className='admin-modal-text'>
            <div className='admin-modal-text-badge'>Чему обучаем:</div>
            {props.event.learn.map((i, k) => {
              return (
                <div key={k} className='admin-modal-text-inner'>
                  {i}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  ) : null;

const EventAddModal = props =>
  props.modal ? (
    <div className='admin-modal'>
      <div className='admin-modal-bg' onClick={() => props.close()}></div>
      <div className='admin-modal-inner'>
        <div className='admin-modal-title'>Добавить событие</div>
        <form action='' className='admin-modal-form'>
          <input
            value={props.form.name}
            onChange={e => props.changeForm('name', e.target.value)}
            type='text'
            placeholder='Название события'
          />
          <textarea
            value={props.form.info}
            onChange={e => props.changeForm('info', e.target.value)}
            name=''
            rows='4'
            placeholder='Описание'
          ></textarea>
          <input
            value={props.form.date}
            onChange={e => props.changeForm('date', e.target.value)}
            type='date'
            placeholder='Дата события'
          />
          <select
            value={props.form.group}
            onChange={e => props.changeForm('group', e)}
          >
            {props.groups.map((i, k) => {
              console.log(i);
              return (
                <option value={i} key={k}>
                  {i.name}
                </option>
              );
            })}
          </select>
          <div className='admin-modal-add'>
            <input
              value={props.toLearn}
              onChange={e => props.changeLearn(e.target.value)}
              type='text'
              placeholder='Чему обучает этот урок'
            />
            <div onClick={() => props.addLearn()}>Добавить</div>
          </div>
          <div className='admin-modal-items'>
            {props.form.learn.map((i, k) => {
              return (
                <div key={k} className='admin-modal-items-i'>
                  {i}
                </div>
              );
            })}
          </div>
          <div
            className='admin-modal-submit'
            onClick={() => props.formSubmit()}
          >
            Сохранить
          </div>
        </form>
      </div>
    </div>
  ) : null;

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      addEvent: false,
      eventForm: {
        name: '',
        date: '',
        info: '',
        learn: [],
        group: null,
      },
      toLearn: '',
      events: [],
      event: null,
      groups: [],
      group: null,
    };

    this.changeEventForm = this.changeEventForm.bind(this);
    this.addEventLearnList = this.addEventLearnList.bind(this);
    this.changeEventLearnList = this.changeEventLearnList.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {};
  }

  changeEventForm(type, text) {
    console.log(text);
    this.setState({
      eventForm: {
        ...this.state.eventForm,
        [type]: text,
      },
    });
  }

  changeEventLearnList(item) {
    this.setState({
      toLearn: item,
    });
  }

  formSubmit() {
    axios
      .post(`/api/event`, this.state.eventForm)
      .then(response => {
        console.log(response);
      })
      .catch(err => {});
  }

  addEventLearnList() {
    const form = this.state.eventForm;
    form.learn.push(this.state.toLearn);

    console.log(form);

    this.setState({
      toLearn: '',
      eventForm: form,
    });
  }

  componentDidMount() {
    axios
      .get(`/api/events`)
      .then(response => {
        this.setState({
          events: response.data,
        });
      })
      .catch(err => {});

    axios
      .get(`/api/groups`)
      .then(response => {
        this.setState({
          groups: response.data,
          eventForm: {
            ...this.state.eventForm,
            group: response.data[0],
          },
        });
      })
      .catch(err => {});
  }

  componentDidUpdate(prevProps) {}

  render() {
    return (
      <Fragment>
        <div className='admin'>
          <div className='admin-top'>
            <div className='container'>
              <div className='admin-top-inner'>
                <div className='admin-top-title'>Кабинет админестратора</div>
              </div>
            </div>
          </div>
          <EventModal
            event={this.state.event}
            close={() => this.setState({ event: null })}
          />
          <EventAddModal
            modal={this.state.addEvent}
            close={() => this.setState({ addEvent: false })}
            form={this.state.eventForm}
            changeForm={this.changeEventForm}
            addLearn={this.addEventLearnList}
            formSubmit={this.formSubmit}
            groups={this.state.groups}
            toLearn={this.state.toLearn}
            changeLearn={this.changeEventLearnList}
          />
          <div className='admin-events'>
            <div className='container'>
              <div className='admin-events-inner'>
                <div className='admin-events-top'>
                  <div className='admin-events-title'>События</div>
                  <div
                    className='admin-events-button'
                    onClick={() => this.setState({ addEvent: true })}
                  >
                    Добавить событие
                  </div>
                </div>
                <div className='admin-events-table'>
                  {this.state.events.map((i, k) => {
                    return (
                      <div key={k} className='admin-events-table-i'>
                        <div className='admin-events-table-i-name'>
                          {k + 1}. {i.name}
                        </div>
                        <div className='admin-events-table-i-buttons'>
                          <div
                            className='admin-events-table-i-watch'
                            onClick={() => this.setState({ event: i })}
                          >
                            Посмотреть
                          </div>
                          <div className='admin-events-table-i-delete'>
                            Удалить
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default (Admin);
