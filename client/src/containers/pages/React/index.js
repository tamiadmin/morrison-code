import React, { Fragment, Component, useLayoutEffect } from 'react';

import './react.scss';
import Form from '../../../components/form';

class ReactPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeCourse: 'Все курсы',
    };
  }

  componentDidMount() {}
  changeModal(type) {
    this.setState({ showmodal: type });
  }

  render() {
    return (
      <Fragment>
        <div className='react'>
          <div className='react-welcome react-welcome-react'>
            <div className='react-welcome-bg'>
              <div className='container'>
                <div className='react-welcome-inner'>
                  <div className='react-welcome-title'>
                    Оффлайн курс по <span>React</span>
                  </div>
                  <div className='react-welcome-text'>
                    Для амбициозных верстальщиков и Junior
                    Frontend-разработчиков, желающих расти
                  </div>
                  <div className='react-welcome-button'>
                    <button>Узнайте подробнее</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end welcome */}
          {/* in */}
          <div className='react-in'>
            <div className='container'>
              <div className='react-in-inner'>
                <div className='react-in-title'>Курс по React – это</div>
                <div className='react-in-items'>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>3</div>
                    <div className='react-in-items-i-text'>
                      месяца. Чтобы стать профессионалом
                    </div>
                  </div>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>3 + 1</div>
                    <div className='react-in-items-i-text'>урока в неделю</div>
                  </div>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>3</div>
                    <div className='react-in-items-i-text'>
                      Проекта в ваше портфолио. Судоку, Кино-ресурс и ваш личный
                      проект
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end in */}
          {/* exp */}
          <div className='react-exp'>
            <div className='container'>
              <div className='react-exp-inner'>
                Мы умеем объяснять сложные вещи понятным языком и знаем, как
                правильно организовать учебный процесс. <br /> <br /> Почему?
                Потому что уже более четырех лет занимаемся образованием в IT.
              </div>
            </div>
          </div>
          {/* end exp */}
          <div className='react-about'>
            <div className='container'>
              <div className='react-about-inner'>
                <div className='react-about-title'>О курсе</div>
                <div className='react-about-items'>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/rocket.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>
                        Теория и практика
                      </div>
                      <div className='react-about-items-i-text'>
                        2 месяца которые проходят в Понедельник, Среду, Субботу.
                        Продолжительность занятия 2 часа. Каждый участник
                        получает максимум внимания. Занятия проходят в
                        интерактивном формате, это не записанное видео, а живое
                        общение в реальном времени. Если вы пропустили занятие
                        или желаете повторить тему — не переживайте! Вам будет
                        предоставлен доступ к урокам других групп. На практику
                        выделяется 3 недели. За это время будут написаны 3
                        персональные приложения, а также все студенты группы
                        могут принять участие в групповой разработке проекта.
                      </div>
                    </div>
                  </div>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/clock.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>Поддержка</div>
                      <div className='react-about-items-i-text'>
                        Помощников достаточно! К твоим услугам преподаватель,
                        ментор и целое коммьюнити — Morrison Chat. В курс
                        включена проверка домашних заданий и тестов, а также
                        индивидуальные занятия с автором курса — поддержки
                        получите столько, сколько вам нужно.
                      </div>
                    </div>
                  </div>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/case.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>
                        Опытные лекторы
                      </div>
                      <div className='react-about-items-i-text'>
                        У нас преподают лекторы с многолетним стажем работы. Они
                        делятся с учениками самой полезной информацией и дают
                        только дельные советы.
                      </div>
                    </div>
                  </div>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/female.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>
                        Для кого курс
                      </div>
                      <div className='react-about-items-i-text'>
                        ✔️ Выпускники "Веб разработчик с нуля"
                        <br />
                        ✔️ Фронтенд-разработчики, которые повышают свой
                        профессиональный уровень <br />
                        ✔️ Бекенд-разработчики, которые переходят в фуллстек
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end about */}
          {/* ready */}
          <div className='react-ready'>
            <div className='container'>
              <div className='react-ready-inner'>
                <div className='react-ready-title'>
                  Как понять, что Вы готовы?
                </div>
                <div className='react-ready-text'>
                  Вы свободно верстаете и умеете пользоваться библиотеками типа
                  jQuery и Bootstrap, или нативным JavaScript. Знаете, как
                  добавить на сайт различные компоненты из библиотек, и у вас
                  есть опыт коммерческой разработки в агентстве, на фрилансе или
                  в каком-либо проекте
                </div>
              </div>
            </div>
          </div>
          {/* end ready */}
          <div className='react-plan'>
            <div className='container'>
              <div className='react-plan-inner'>
                <div className='react-plan-title'>План курса</div>
                <div className='react-plan-items'>
                  <div className='react-plan-items-i'>
                    <div className='react-plan-items-i-num'>1 этап</div>
                    <div className='react-plan-items-i-text'>
                      Знакомство с React и старт работы над библиотекой
                      компонентов
                    </div>
                  </div>
                  <div className='react-plan-items-i'>
                    <div className='react-plan-items-i-num'>2 этап</div>
                    <div className='react-plan-items-i-text'>
                      Продолжаем работать над библиотекой компонентов —
                      добавляем более сложные
                    </div>
                  </div>
                  <div className='react-plan-items-i'>
                    <div className='react-plan-items-i-num'>3 этап</div>
                    <div className='react-plan-items-i-text'>
                      Учимся собирать данные с формы и разрабатываем
                      соответствующие компоненты
                    </div>
                  </div>
                  <div className='react-plan-items-i'>
                    <div className='react-plan-items-i-num'>4 этап</div>
                    <div className='react-plan-items-i-text'>
                      State Management на примере разных библиотек (подробно
                      Redux)
                    </div>
                  </div>
                  <div className='react-plan-items-i'>
                    <div className='react-plan-items-i-num'>5 этап</div>
                    <div className='react-plan-items-i-text'>
                      Стартуем итоговый проект сервис рекомендаций фильмов
                      (основа и первые запросы к серверу)
                    </div>
                  </div>
                  <div className='react-plan-items-i'>
                    <div className='react-plan-items-i-num'>6 этап</div>
                    <div className='react-plan-items-i-text'>
                      Завершаем выпускной проект и получаем рекомендации по
                      оптимизации и деплою проекта
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* founder */}
          {/* <div className='react-founder'>
            <div className='container'>
              <div className='react-founder-inner'>
                <div className='react-founder-content'>
                  <div className='react-founder-title'>Добро пожаловать!</div>
                  <div className='react-founder-text'>
                    Я верю, что учеба может быть продуктивной, но при этом
                    увлекательной. Что знания должны быть практичными. А любой
                    профессией можно овладеть в сжатые сроки. Именно поэтому я
                    основал Академию Morrison. Мы исповедуем принцип
                    доступности. Доступным языком о самом сложном. Доступ к
                    нашим онлайн-курсам из любой точки мира. А главное –
                    быстрый, но эффективный формат обучения. Мечта гораздо
                    ближе, чем вы думаете!
                  </div>
                  <div className='react-founder-name'>Несмелов Кирилл</div>
                  <div className='react-founder-job'>
                    Генеральный директор Morrison
                  </div>
                </div>
              </div>
            </div>
          </div> */}
          {/* end founder */}
          {/* team */}
          <div className='react-team'>
            <div className='container'>
              <div className='react-team-inner'>
                <div className='react-team-title'>Преподователи курса</div>
                <div className='react-team-items'>
                  <div
                    className='react-team-items-i'
                    style={{
                      backgroundImage: 'url(/images/team/takhir.jpg)',
                    }}
                  >
                    <div className='react-team-items-i-cover'>
                      <div className='react-team-items-i-type'>
                        Куратор курсов
                      </div>
                      <div className='react-team-items-i-name'>
                        Мунарбеков Тахир
                      </div>
                      <div className='react-team-items-i-text'>
                        Каждый день мы стараемся сделать лучший контент для
                        нашей школы. Каждый курс мы проходим всей командой, для
                        того чтобы решить достоин он наших учеников или нет.
                      </div>
                    </div>
                  </div>
                  <div
                    className='react-team-items-i'
                    style={{
                      backgroundImage: 'url(/images/team/almas.jpg)',
                    }}
                  >
                    <div className='react-team-items-i-cover'>
                      <div className='react-team-items-i-type'>
                        Куратор курсов
                      </div>
                      <div className='react-team-items-i-name'>
                        Ибраев Алмас
                      </div>
                      <div className='react-team-items-i-text'>
                        Вы можете достичь результата только в том случае, если
                        идете к цели вместе с опытным куратором и наставником.
                        Добро пожаловать к нам. Мы рады что вы с нами
                      </div>
                    </div>
                  </div>
                  <div
                    className='react-team-items-i'
                    style={{
                      backgroundImage: 'url(/images/team/stas.jpg)',
                    }}
                  >
                    <div className='react-team-items-i-cover'>
                      <div className='react-team-items-i-type'>
                        Куратор курсов
                      </div>
                      <div className='react-team-items-i-name'>
                        Югай Станислав
                      </div>
                      <div className='react-team-items-i-text'>
                        Я рад, что помогаю людям менять жизнь к лучшему. Освоить
                        за пару месяцев новую профессию. Выйти на достойный
                        заработок. Открыть для себя увлекательный мир
                        программирования. Самое ценное для меня – искренняя
                        благодарность студентов.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end team */}
          {/* select */}
          <div className='react-select'>
            <div className='container'>
              <div className='react-select-inner'>
                <div className='react-select-content'>
                  <div className='react-select-title'>
                    Приглашаем вас на бесплатный пробный урок
                  </div>
                  <div className='react-select-text'>
                    На пробном уроке вы узнаете подробнее о курсе и покажем
                    правильное направление к вашему результату
                  </div>
                  {/* <div className='react-select-button'>
                    <button>Выбрать курс</button>
                  </div> */}
                </div>
                <div className='react-select-form'>
                  <div className='react-select-form-title'>
                    Записаться на урок
                  </div>
                  <Form button={true} />
                </div>
              </div>
            </div>
          </div>
          {/* end select */}
          {/* questions */}
          <div className='react-q'>
            <div className='container'>
              <div className='react-q-inner'>
                <div className='react-q-content'>
                  <div className='react-q-title'>Остались вопросы?</div>
                  <div className='react-q-text'>Пишите или звоните</div>
                </div>
                <div className='react-q-items'>
                  <div className='react-q-i'>
                    <div className='react-q-i-name'>Телефон:</div>
                    <div className='react-q-i-number'>
                      <a href='tel:87064003644'>+7 706 400-36-44</a>
                    </div>
                    <div className='react-q-i-number'>
                      <a href='tel:87064126705'>+7 706 412-67-05</a>
                    </div>
                  </div>
                  <div className='react-q-i'>
                    <div className='react-q-i-name'>E-mail:</div>
                    <div className='react-q-i-number'>
                      <a href='mailto:code@morrison.kz' target='_blank'>
                        code@morrison.kz
                      </a>
                    </div>
                  </div>
                  <div className='react-q-i'>
                    <div className='react-q-i-name'>Телеграмм:</div>
                    <div className='react-q-i-number'>
                      <a
                        target='_blank'
                        href='https://t.me/joinchat/AAAAAEJbiJqU4bnmEEy5gA'
                      >
                        @morrisoncode
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end questions */}
          {/* chat */}
          {/* <div className='react-chat'>
            <div className='container'>
              <div className='react-chat-inner'>
                <div className='react-chat-title'>Чат для веб-дизайнеров</div>
                <div className='react-chat-text'>
                  Здесь вы можете задать любой вопрос о веб-дизайне и получить
                  <br />
                  ответ наших специалистов
                </div>
                <div className='react-chat-button'>
                  <button>Перейти в чат</button>
                </div>
              </div>
            </div>
          </div> */}
          {/* end chat */}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default ReactPage;
