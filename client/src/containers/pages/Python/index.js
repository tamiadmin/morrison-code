import React, { Fragment, Component } from 'react';

import Form from '../../../components/form';

class PythonPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeCourse: 'Все курсы',
    };
  }

  componentDidMount() {}
  changeModal(type) {
    this.setState({ showmodal: type });
  }

  render() {
    return (
      <Fragment>
        <div className='react'>
          <div className='react-welcome react-welcome-python'>
            <div className='react-welcome-bg'>
              <div className='container'>
                <div className='react-welcome-inner'>
                  <div className='react-welcome-title'>
                    <span>Python</span> язык будущего
                  </div>
                  <div className='react-welcome-text'>
                    Научитесь с нуля программировать на Python
                  </div>
                  <div className='react-welcome-button'>
                    <button>Узнайте подробнее</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end welcome */}
          {/* in */}
          <div className='react-in'>
            <div className='container'>
              <div className='react-in-inner'>
                <div className='react-in-title'>Курс по React – это</div>
                <div className='react-in-items'>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>3</div>
                    <div className='react-in-items-i-text'>
                      месяца интенсивных занятий с практикующими разработчиками
                    </div>
                  </div>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>3 + 1</div>
                    <div className='react-in-items-i-text'>урока в неделю</div>
                  </div>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>4</div>
                    <div className='react-in-items-i-text'>
                      для вашего портфолио по итогам обучения
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end in */}
          {/* exp */}
          <div className='react-exp'>
            <div className='container'>
              <div className='react-exp-inner'>
                Мы умеем объяснять сложные вещи понятным языком и знаем, как
                правильно организовать учебный процесс. <br /> <br /> Почему?
                Потому что уже более четырех лет занимаемся образованием в IT.
              </div>
            </div>
          </div>
          {/* end exp */}
          <div className='react-about'>
            <div className='container'>
              <div className='react-about-inner'>
                <div className='react-about-title'>О курсе</div>
                <div className='react-about-items'>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/rocket.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>
                        Теория и практика
                      </div>
                      <div className='react-about-items-i-text'>
                        После каждой лекции будете делать практические задания,
                        а затем получать развёрнутую обратную связь. Так
                        наполните портфолио.
                      </div>
                    </div>
                  </div>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/clock.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>Поддержка</div>
                      <div className='react-about-items-i-text'>
                        Преподаватели будут постоянно на связи — в общем чате в
                        Telegram. <br />А еще у каждой группы будут координаторы
                        и аспиранты, которые решат организационные вопросы.
                      </div>
                    </div>
                  </div>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/case.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>
                        Трудоустройство или стажировка
                      </div>
                      <div className='react-about-items-i-text'>
                        Наш центр развития карьеры поможет составить резюме и
                        подготовит к собеседованию. Вы можете пройти стажировку
                        в проектах передовых IT-компаний Казахстана.
                      </div>
                    </div>
                  </div>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/female.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>
                        Для кого курс
                      </div>
                      <div className='react-about-items-i-text'>
                        ✔️ Новичкам - Наш курс отличная возможность освоить
                        новую сферу. Вы получите структурированную информацию,
                        готовое портфолио и помощь в дальнейшем трудоустройстве.
                        <br />
                        ✔️ Веб-разработчикам - Если вы сейчас используете другой
                        язык, то знание Python позволит повысить вашу стоимость
                        на рынке и продвинуться по карьерной лестнице.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end about */}
          {/* ready */}
          <div className='react-ready'>
            <div className='container'>
              <div className='react-ready-inner'>
                <div className='react-ready-title'>
                  Насколько популярен Python
                </div>
                <div className='react-ready-text'>
                  Python входит в топ-10 самых востребованных языков
                  программирования (по данным Stack Overflow). Он открывает путь
                  в топовые IT-компании: Google, Pixar, Youtube, Instagram,
                  Nasa, Intel, Pinterest используют именно его. <br /> <br />
                  После курса вы сможете устроиться в компанию, где создают
                  интерактивные веб-сервисы на Python, а таких очень много: 3305
                  вакансий для python-разработчика открыто прямо сейчас на
                  hh.ru.
                  <br /> <br /> При этом Python отлично подойдет в качестве
                  первого языка: он лаконичный и простой.
                </div>
              </div>
            </div>
          </div>
          {/* end ready */}
          <div className='react-plan'>
            <div className='container'>
              <div className='react-plan-inner'>
                <div className='react-plan-title'>План курса</div>
                <ul data-uk-accordion>
                  <li>
                    <div className='uk-accordion-title react-plan-opener-title '>
                      Git — система контроля версий
                    </div>
                    <ul className='uk-accordion-content react-plan-opener-content'>
                      <li>Предназначение системы контроля версий</li>
                      <li>
                        Основные операции (фиксация и откат изменений, поиск,
                        история)
                      </li>
                      <li>Работа с сервисом GitHub</li>
                      <li>Ветки, слияние веток и разрешение конфликтов</li>
                      <li>Командная работа</li>
                    </ul>
                  </li>
                  <li>
                    <div className='uk-accordion-title react-plan-opener-title '>
                      Основы языка программирования Python
                    </div>
                    <ul className='uk-accordion-content react-plan-opener-content'>
                      <li>
                        Python, знакомство с консолью. Условные конструкции
                      </li>
                      <li>Циклы. Типы данных. Коллекции данных</li>
                      <li>
                        Функции — использование встроенных и создание
                        собственных
                      </li>
                      <li>Классы и их применение в Python</li>
                      <li>Открытие и чтение файла, запись в файл</li>
                      <li>Работа с разными форматами данных</li>
                      <li>Работа с папками, путями. Вызов других программ</li>
                      <li>Работа с библиотекой requests, http-запросы</li>
                      <li>Инструменты для оперативной работы с данными</li>
                      <li>Инструменты для визуализации и изучения данных</li>
                    </ul>
                  </li>
                  <li>
                    <div className='uk-accordion-title react-plan-opener-title '>
                      Работа с данными
                    </div>
                    <ul className='uk-accordion-content react-plan-opener-content'>
                      <li>Профессиональное использование языка Python</li>
                      <li>Базы данных</li>
                      <li>Анализ данных</li>
                      <li>Тесты для кода</li>
                    </ul>
                  </li>
                  <li>
                    <div className='uk-accordion-title react-plan-opener-title '>
                      Создание функциональных веб-приложений с использованием
                      Django
                    </div>
                    <ul className='uk-accordion-content react-plan-opener-content'>
                      <li>Обработка запросов</li>
                      <li>
                        Динамическое формирование страниц на основе шаблонов
                      </li>
                      <li>Базы данных</li>
                      <li>Взаимодействие с сайтом</li>
                      <li>Персонализация сайта</li>
                      <li>Создание и поддержка проекта</li>
                    </ul>
                  </li>
                  <li>
                    <div className='uk-accordion-title react-plan-opener-title '>
                      Карьера в Python-разработке
                    </div>
                    <ul className='uk-accordion-content react-plan-opener-content'>
                      <li>Python-разработчик: задачи, работа в команде</li>
                      <li>Поиск работы: компания vs фриланс</li>
                      <li>Карьерная траектория: из студента в senior</li>
                      <li>Первое собеседование</li>
                      <li>
                        Тренды python-разработки: за какими ресурсами следить
                      </li>
                      <li>Резюме, сопроводительное письмо, портфолио</li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          {/* founder */}
          {/* <div className='react-founder'>
            <div className='container'>
              <div className='react-founder-inner'>
                <div className='react-founder-content'>
                  <div className='react-founder-title'>Добро пожаловать!</div>
                  <div className='react-founder-text'>
                    Я верю, что учеба может быть продуктивной, но при этом
                    увлекательной. Что знания должны быть практичными. А любой
                    профессией можно овладеть в сжатые сроки. Именно поэтому я
                    основал Академию Morrison. Мы исповедуем принцип
                    доступности. Доступным языком о самом сложном. Доступ к
                    нашим онлайн-курсам из любой точки мира. А главное –
                    быстрый, но эффективный формат обучения. Мечта гораздо
                    ближе, чем вы думаете!
                  </div>
                  <div className='react-founder-name'>Несмелов Кирилл</div>
                  <div className='react-founder-job'>
                    Генеральный директор Morrison
                  </div>
                </div>
              </div>
            </div>
          </div> */}
          {/* end founder */}
          {/* team */}
          <div className='react-team'>
            <div className='container'>
              <div className='react-team-inner'>
                <div className='react-team-title'>Преподаватели курса</div>
                <div className='react-team-items'>
                  <div
                    className='react-team-items-i'
                    style={{
                      backgroundImage: 'url(/images/team/takhir.jpg)',
                    }}
                  >
                    <div className='react-team-items-i-cover'>
                      <div className='react-team-items-i-type'>
                        Куратор курсов
                      </div>
                      <div className='react-team-items-i-name'>
                        Мунарбекоа Тахир
                      </div>
                      <div className='react-team-items-i-text'>
                        Каждый день мы стараемся сделать лучший контент для
                        нашей школы. Каждый курс мы проходим всей командой, для
                        того чтобы решить достоин он наших учеников или нет.
                      </div>
                    </div>
                  </div>
                  <div
                    className='react-team-items-i'
                    style={{
                      backgroundImage: 'url(/images/team/almas.jpg)',
                    }}
                  >
                    <div className='react-team-items-i-cover'>
                      <div className='react-team-items-i-type'>
                        Куратор курсов
                      </div>
                      <div className='react-team-items-i-name'>
                        Ибраев Алмас
                      </div>
                      <div className='react-team-items-i-text'>
                        Вы можете достичь результата только в том случае, если
                        идете к цели вместе с опытным куратором и наставником.
                        Добро пожаловать к нам. Мы рады что вы с нами
                      </div>
                    </div>
                  </div>
                  <div
                    className='react-team-items-i'
                    style={{
                      backgroundImage: 'url(/images/team/stas.jpg)',
                    }}
                  >
                    <div className='react-team-items-i-cover'>
                      <div className='react-team-items-i-type'>
                        Куратор курсов
                      </div>
                      <div className='react-team-items-i-name'>
                        Югай Станислав
                      </div>
                      <div className='react-team-items-i-text'>
                        Я рад, что помогаю людям менять жизнь к лучшему. Освоить
                        за пару месяцев новую профессию. Выйти на достойный
                        заработок. Открыть для себя увлекательный мир
                        программирования. Самое ценное для меня – искренняя
                        благодарность студентов.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end team */}
          {/* select */}
          <div className='react-select'>
            <div className='container'>
              <div className='react-select-inner'>
                <div className='react-select-content'>
                  <div className='react-select-title'>
                    Приглашаем вас на бесплатный пробный урок
                  </div>
                  <div className='react-select-text'>
                    На пробном уроке вы узнаете подробнее о курсе и покажем
                    правильное направление к вашему результату
                  </div>
                  {/* <div className='react-select-button'>
                    <button>Выбрать курс</button>
                  </div> */}
                </div>
                <div className='react-select-form'>
                  <div className='react-select-form-title'>
                    Записаться на урок
                  </div>
                  <Form button={true} />
                </div>
              </div>
            </div>
          </div>
          {/* end select */}
          {/* questions */}
          <div className='react-q'>
            <div className='container'>
              <div className='react-q-inner'>
                <div className='react-q-content'>
                  <div className='react-q-title'>Остались вопросы?</div>
                  <div className='react-q-text'>Пишите или звоните</div>
                </div>
                <div className='react-q-items'>
                  <div className='react-q-i'>
                    <div className='react-q-i-name'>Телефон:</div>
                    <div className='react-q-i-number'>
                      <a href='tel:87064003644'>+7 706 400-36-44</a>
                    </div>
                    <div className='react-q-i-number'>
                      <a href='tel:87064126705'>+7 706 412-67-05</a>
                    </div>
                  </div>
                  <div className='react-q-i'>
                    <div className='react-q-i-name'>E-mail:</div>
                    <div className='react-q-i-number'>
                      <a href='mailto:code@morrison.kz' target='_blank'>
                        code@morrison.kz
                      </a>
                    </div>
                  </div>
                  <div className='react-q-i'>
                    <div className='react-q-i-name'>Телеграмм:</div>
                    <div className='react-q-i-number'>
                      <a
                        target='_blank'
                        href='https://t.me/joinchat/AAAAAEJbiJqU4bnmEEy5gA'
                      >
                        @morrisoncode
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end questions */}
          {/* chat */}
          {/* <div className='react-chat'>
            <div className='container'>
              <div className='react-chat-inner'>
                <div className='react-chat-title'>Чат для веб-дизайнеров</div>
                <div className='react-chat-text'>
                  Здесь вы можете задать любой вопрос о веб-дизайне и получить
                  <br />
                  ответ наших специалистов
                </div>
                <div className='react-chat-button'>
                  <button>Перейти в чат</button>
                </div>
              </div>
            </div>
          </div> */}
          {/* end chat */}
        </div>
      </Fragment>
    );
  }
}

export default PythonPage;
