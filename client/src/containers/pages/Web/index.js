import React, { Fragment, Component } from 'react';
import Form from '../../../components/form';

class WebPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeCourse: 'Все курсы',
    };
  }

  componentDidMount() {}
  changeModal(type) {
    this.setState({ showmodal: type });
  }

  render() {
    return (
      <Fragment>
        <div className='react'>
          <div className='react-welcome react-welcome-web'>
            <div className='react-welcome-bg'>
              <div className='container'>
                <div className='react-welcome-inner'>
                  <div className='react-welcome-title'>
                    Курс по <br />
                    <span>Web разработке</span>
                  </div>
                  <div className='react-welcome-text'>
                    Прокачайся с нуля до junior front-end developer всего за 2
                    месяцев
                  </div>
                  <div className='react-welcome-button'>
                    <button>Узнайте подробнее</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end welcome */}
          {/* in */}
          <div className='react-in'>
            <div className='container'>
              <div className='react-in-inner'>
                <div className='react-in-title'>Начни с Web</div>
                <div className='react-in-items'>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>3</div>
                    <div className='react-in-items-i-text'>
                      месяца. Чтобы стать разработчиком
                    </div>
                  </div>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>3 + 1</div>
                    <div className='react-in-items-i-text'>
                      урока по 2 часа в неделю
                    </div>
                  </div>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>3</div>
                    <div className='react-in-items-i-text'>
                      Проекта в ваше портфолио. Судоку, Кино-ресурс и ваш личный
                      проект
                    </div>
                  </div>
                </div>
                <div className='react-in-items'>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>10+</div>
                    <div className='react-in-items-i-text'>
                      Часов на реальный проектов
                    </div>
                  </div>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>20</div>
                    <div className='react-in-items-i-text'>
                      Супер интересных домашних заданий
                    </div>
                  </div>
                  <div className='react-in-items-i'>
                    <div className='react-in-items-i-title'>8</div>
                    <div className='react-in-items-i-text'>
                      Дополнительных уроков от профессионалов своего дела
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end in */}
          <div className='react-about'>
            <div className='container'>
              <div className='react-about-inner'>
                <div className='react-about-title'>О курсе</div>
                <div className='react-about-items'>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/rocket.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>
                        Теория и практика
                      </div>
                      <div className='react-about-items-i-text'>
                        После каждого занятия — практическое задание которое
                        поможет закрепить задание. После каждой пройденной темы
                        - собственный проект. Правильные решения с подробными
                        комментариями. Вопросы можно задать в коммьюнити.
                      </div>
                    </div>
                  </div>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/clock.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>Поддержка</div>
                      <div className='react-about-items-i-text'>
                        Помощь и поддержку преподавателей и координатора курса
                        можно получить в закрытой группе на Telegram. Там же вы
                        можете пообщаться с другими студентами курса.
                      </div>
                    </div>
                  </div>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/case.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>Карьера</div>
                      <div className='react-about-items-i-text'>
                        Выберите специализацию, которая вам больше по душе:
                        веб-разработка, front-end, python или android, получите
                        новую профессию и постройте карьеру разработчика.
                      </div>
                    </div>
                  </div>
                  <div className='react-about-items-i'>
                    <div className='react-about-items-i-img'>
                      <img src='/images/emoji/female.png' alt='' />
                    </div>
                    <div className='react-about-items-i-content'>
                      <div className='react-about-items-i-title'>
                        Для кого курс
                      </div>
                      <div className='react-about-items-i-text'>
                        ✔️ Выпускники "Веб разработчик с нуля"
                        <br />
                        ✔️ Фронтенд-разработчики, которые повышают свой
                        профессиональный уровень <br />
                        ✔️ Бекенд-разработчики, которые переходят в фуллстек
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end about */}
          {/* ready */}
          <div className='react-ready'>
            <div className='container'>
              <div className='react-ready-inner'>
                <div className='react-ready-title'>
                  Отличный старт для погружения в разработку сайтов
                </div>
                <div className='react-ready-text'>
                  Знания основ HTML и CSS нужны всем, кто хочет работать с
                  вебом, независимо от того, планируете ли вы стать
                  верстальщиком, frontend-разработчиком или
                  backend-разработчиком. Дизайнерам, контент-менеджерам,
                  интернет-маркетологам и руководителям проектов также
                  пригодится умение внести изменения на сайте.
                </div>
              </div>
            </div>
          </div>
          {/* end ready */}
          <div className='react-plan'>
            <div className='container'>
              <div className='react-plan-inner'>
                <div className='react-plan-title'>План курса</div>
                <ul data-uk-accordion>
                  <li>
                    <div className='uk-accordion-title react-plan-opener-title '>
                      Этап 1. HTML & CSS.
                    </div>
                    <ul className='uk-accordion-content react-plan-opener-content'>
                      <li>- Как устроен браузер. Процесс разработки сайта.</li>
                      <li>- Что такое HTML и CSS.</li>
                      <li>- Какие инструменты использует верстальщик.</li>
                      <li>- Введение в HTML. Что такое теги и атрибуты.</li>
                      <li>- Базовая структура html документа.</li>
                      <li>- Стиль кода.</li>
                      <li>- Знакомство с панелью разработчика.</li>
                      <li>- Разберемся в блочных и строчных элементах.</li>
                      <li>- Знакомство c CSS. Синтаксис CSS.</li>
                      <li>- Разновидности селекторов.</li>
                      <li>
                        - Вложенность, наследование и группирование селекторов.
                      </li>
                      <li>- Зачем нужно много классов.</li>
                      <li>
                        - Создаем базовые элементы. Кнопки, текстовые блоки,
                        шапка, футер.
                      </li>
                      <li>- Введение в FlexBox.</li>
                      <li>- Строим стандартную сетку для блоков.</li>
                      <li>
                        - Знакомство с позиционированием элементов и где его
                        можно использовать.
                      </li>
                      <li>
                        - Псевдоклассы и псевдоэлементы. Что для чего нужно?
                      </li>
                    </ul>
                  </li>
                  <li>
                    <div className='uk-accordion-title react-plan-opener-title '>
                      Этап 2. HTML & CSS первый Landing.
                    </div>
                    <ul className='uk-accordion-content react-plan-opener-content'>
                      <li>- Введение в Git.</li>
                      <li>- Верстка первого макета. Landing page.</li>
                      <li>
                        - Подключаем к работе систему контроля версий GIT.
                        Работаем по git flow.
                      </li>
                      <li>- Развиваем модульный или компонентный подход.</li>
                      <li>
                        - Подключение шрифтов. Использование шрифтовых иконок.
                      </li>
                      <li>- Знакомство с элементами форм.</li>
                      <li>- Использование и стилизация первых скриптов.</li>
                      <li>- Подключаем google карты.</li>
                      <li>
                        - Первые прикосновения к анимации. Базовое анимирование
                        свойств и появление элементов.
                      </li>
                      <li>
                        - Погружаемся в адаптив и мобильную верстку. Медиа
                        запросы. Что такое Viewport?
                      </li>
                      <li>
                        - Как правильно проверять адаптив. Какие нюансы есть в
                        мобильных девайсах.
                      </li>
                      <li>
                        - Знакомство с тенями, и что с их помощью можно сделать.
                      </li>
                      <li>- Пишем документацию по проекту.</li>
                    </ul>
                  </li>
                  <li>
                    <div className='uk-accordion-title react-plan-opener-title '>
                      Этап 3. HTML & CSS Продвинутый.
                    </div>
                    <ul className='uk-accordion-content react-plan-opener-content'>
                      <li>- Введение в Grid.</li>
                      <li>- Основы по вёрстке Email писем.</li>
                      <li>- Фичи chrome dev tools.</li>
                      <li>- Базовые понятия по sео оптимизации.</li>
                      <li>- Тестирование верстки.</li>
                    </ul>
                  </li>
                  <li>
                    <div className='uk-accordion-title react-plan-opener-title '>
                      Этап 4. JavaScript.
                    </div>
                    <ul className='uk-accordion-content react-plan-opener-content'>
                      <li>- Настройка окружения (Visual Studio Code).</li>
                      <li>- Подключение скриптов.</li>
                      <li>- Типы данных.</li>
                      <li>- Переменные.</li>
                      <li>- Преобразование типов.</li>
                      <li>- Числа.</li>
                      <li>- Строки.</li>
                      <li>- Template string.</li>
                      <li>- Объекты.</li>
                      <li>- Условные операторы.</li>
                      <li>- Тернарный оператор. Конструкция Switch case.</li>
                      <li>- Циклы.</li>
                      <li>- Введение в функции.</li>
                      <li>- Методы массивов.</li>
                    </ul>
                  </li>
                  <li>
                    <div className='uk-accordion-title react-plan-opener-title '>
                      Этап 5. JavaScript Продвинутый
                    </div>
                    <ul className='uk-accordion-content react-plan-opener-content'>
                      <li>- Функции высшего порядка.</li>
                      <li>- This.</li>
                      <li>- Arrow function.</li>
                      <li>- Перебирающие методы массивов.</li>
                      <li>- Замыкание.</li>
                      <li>
                        - Методы объектов. Advanced Object. Object.entries,
                        Object.keys, Object.values.
                      </li>
                      <li>- Объекты обертки.</li>
                      <li>- Деструктуризация, оператор rest/spred.</li>
                      <li>- Object descriptor.</li>
                      <li>- Введение в DOM и BOM.</li>
                      <li>- Работа с атрибутами.</li>
                      <li>
                        - Работа с содержимым элемента, создание, добавление и
                        удаление элементов.
                      </li>
                      <li>- События.</li>
                      <li>- Всплытие и погружение события.</li>
                      <li>- Первый проект Todo List.</li>
                      <li>
                        - CSS переменные и работа с ними через JavaScript.
                      </li>
                      <li>
                        - Введение в асинхронность. Event Loop. Как его понять?
                      </li>
                      <li>- Введение в AJAX.</li>
                      <li>- AJAX отладка.</li>
                      <li>- POST запросы и не только.</li>
                      <li>- Что такое CORS?</li>
                      <li>- Обработка ошибок при запросах.</li>
                      <li>- LocalStorage.</li>
                      <li>- Проект News App Intro.</li>
                      <li>- Проект News App.</li>
                      <li>- Введение в ООП. Конструкторы и классы.</li>
                      <li>- Прототипы в ES5.</li>
                      <li>- Наследование ES5.</li>
                      <li>- Классы ES6.</li>
                      <li>- Наследование ES6.</li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          {/* founder */}
          {/* <div className='react-founder'>
            <div className='container'>
              <div className='react-founder-inner'>
                <div className='react-founder-content'>
                  <div className='react-founder-title'>Добро пожаловать!</div>
                  <div className='react-founder-text'>
                    Я верю, что учеба может быть продуктивной, но при этом
                    увлекательной. Что знания должны быть практичными. А любой
                    профессией можно овладеть в сжатые сроки. Именно поэтому я
                    основал Академию Morrison. Мы исповедуем принцип
                    доступности. Доступным языком о самом сложном. Доступ к
                    нашим онлайн-курсам из любой точки мира. А главное –
                    быстрый, но эффективный формат обучения. Мечта гораздо
                    ближе, чем вы думаете!
                  </div>
                  <div className='react-founder-name'>Несмелов Кирилл</div>
                  <div className='react-founder-job'>
                    Генеральный директор Morrison
                  </div>
                </div>
              </div>
            </div>
          </div> */}
          {/* end founder */}
          {/* team */}
          <div className='react-team'>
            <div className='container'>
              <div className='react-team-inner'>
                <div className='react-team-title'> Преподаватели курса</div>
                <div className='react-team-items'>
                  <div
                    className='react-team-items-i'
                    style={{
                      backgroundImage: 'url(/images/team/takhir.jpg)',
                    }}
                  >
                    <div className='react-team-items-i-cover'>
                      <div className='react-team-items-i-type'>
                        Куратор курсов
                      </div>
                      <div className='react-team-items-i-name'>
                        Мунарбеков Тахир
                      </div>
                      <div className='react-team-items-i-text'>
                        Каждый день мы стараемся сделать лучший контент для
                        нашей школы. Каждый курс мы проходим всей командой, для
                        того чтобы решить достоин он наших учеников или нет.
                      </div>
                    </div>
                  </div>
                  <div
                    className='react-team-items-i'
                    style={{
                      backgroundImage: 'url(/images/team/almas.jpg)',
                    }}
                  >
                    <div className='react-team-items-i-cover'>
                      <div className='react-team-items-i-type'>
                        Куратор курсов
                      </div>
                      <div className='react-team-items-i-name'>
                        Ибраев Алмас
                      </div>
                      <div className='react-team-items-i-text'>
                        Вы можете достичь результата только в том случае, если
                        идете к цели вместе с опытным куратором и наставником.
                        Добро пожаловать к нам. Мы рады что вы с нами
                      </div>
                    </div>
                  </div>
                  <div
                    className='react-team-items-i'
                    style={{
                      backgroundImage: 'url(/images/team/stas.jpg)',
                    }}
                  >
                    <div className='react-team-items-i-cover'>
                      <div className='react-team-items-i-type'>
                        Куратор курсов
                      </div>
                      <div className='react-team-items-i-name'>
                        Югай Станислав
                      </div>
                      <div className='react-team-items-i-text'>
                        Я рад, что помогаю людям менять жизнь к лучшему. Освоить
                        за пару месяцев новую профессию. Выйти на достойный
                        заработок. Открыть для себя увлекательный мир
                        программирования. Самое ценное для меня – искренняя
                        благодарность студентов.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end team */}
          {/* select */}
          <div className='react-select'>
            <div className='container'>
              <div className='react-select-inner'>
                <div className='react-select-content'>
                  <div className='react-select-title'>
                    Приглашаем вас на бесплатный пробный урок
                  </div>
                  <div className='react-select-text'>
                    На пробном уроке вы узнаете подробнее о курсе и покажем
                    правильное направление к вашему результату
                  </div>
                  {/* <div className='react-select-button'>
                    <button>Выбрать курс</button>
                  </div> */}
                </div>
                <div className='react-select-form'>
                  <div className='react-select-form-title'>
                    Записаться на урок
                  </div>
                  <Form button={true} />
                </div>
              </div>
            </div>
          </div>
          {/* end select */}
          {/* questions */}
          <div className='react-q'>
            <div className='container'>
              <div className='react-q-inner'>
                <div className='react-q-content'>
                  <div className='react-q-title'>Остались вопросы?</div>
                  <div className='react-q-text'>Пишите или звоните</div>
                </div>
                <div className='react-q-items'>
                  <div className='react-q-i'>
                    <div className='react-q-i-name'>Телефон:</div>
                    <div className='react-q-i-number'>
                      <a href='tel:87064003644'>+7 706 400-36-44</a>
                    </div>
                    <div className='react-q-i-number'>
                      <a href='tel:87064126705'>+7 706 412-67-05</a>
                    </div>
                  </div>
                  <div className='react-q-i'>
                    <div className='react-q-i-name'>E-mail:</div>
                    <div className='react-q-i-number'>
                      <a href='mailto:code@morrison.kz' target='_blank'>
                        code@morrison.kz
                      </a>
                    </div>
                  </div>
                  <div className='react-q-i'>
                    <div className='react-q-i-name'>Телеграмм:</div>
                    <div className='react-q-i-number'>
                      <a
                        target='_blank'
                        href='https://t.me/joinchat/AAAAAEJbiJqU4bnmEEy5gA'
                      >
                        @morrisoncode
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end questions */}
          {/* chat */}
          {/* <div className='react-chat'>
            <div className='container'>
              <div className='react-chat-inner'>
                <div className='react-chat-title'>Чат для веб-дизайнеров</div>
                <div className='react-chat-text'>
                  Здесь вы можете задать любой вопрос о веб-дизайне и получить
                  <br />
                  ответ наших специалистов
                </div>
                <div className='react-chat-button'>
                  <button>Перейти в чат</button>
                </div>
              </div>
            </div>
          </div> */}
          {/* end chat */}
        </div>
      </Fragment>
    );
  }
}

export default WebPage;
