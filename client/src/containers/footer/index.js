import React, { Fragment, Component } from 'react';
import { Link } from 'react-router-dom';

import './footer.scss';

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showmenu: false,
    };

    this.showMenu = this.showMenu.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {};
  }

  componentDidMount() {}

  componentDidUpdate(prevProps) {}

  showMenu() {
    this.setState({ showmenu: !this.state.showmenu });
  }

  render() {
    return (
      <Fragment>
        <div className='footer'>
          <div className='container'>
            <div className='footer-inner'>
              {/* col */}
              <div className='footer-col'>
                <div className='footer-logo'>
                  <img src='/images/logo-black.png' alt='' />
                </div>
                <div className='footer-founded'>
                  2019 - 2020 © Morrison Academy
                </div>
                <div className='footer-links'>
                  <div className='contact-info-social-title'>
                    Наши соц сети:
                  </div>
                  <div className='contact-info-social-i'>
                    <a
                      href='https://www.instagram.com/morrisoncode_almaty/'
                      class='uk-icon-button  uk-margin-small-right'
                      uk-icon='icon: instagram ; '
                    ></a>
                    <a
                      href='https://api.whatsapp.com/send?phone=87064003644&text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5!%20!!%20%D0%A3%20%D0%BC%D0%B5%D0%BD%D1%8F%20%D0%B5%D1%81%D1%82%D1%8C%20%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81'
                      class='uk-icon-button'
                      uk-icon='icon: whatsapp ; '
                    ></a>
                  </div>
                </div>
              </div>
              {/* col */}
              {/* end col */}
              <div className='footer-col'>
                <div className='footer-col-title'> Основное</div>
                <div className='footer-col-i'>
                  <Link to='/'>Главная</Link>
                </div>
                <div className='footer-col-i'>
                  <Link to='/courses'>Курсы</Link>
                </div>
                <div className='footer-col-i'>
                  <Link to='/events'>События</Link>
                </div>
                <div className='footer-col-i'>
                  <Link to='/about'>Об академии</Link>
                </div>
                <div className='footer-col-i'>
                  <Link to='/contact'>Контакты</Link>
                </div>
              </div>
              <div className='footer-col'>
                <div className='footer-col-title'>Курсы </div>
                <div className='footer-col-i'>
                  <Link to='/courses'>Программирование </Link>
                </div>
                <div className='footer-col-i'>
                  <Link to='/courses'>Языковая школа</Link>
                </div>
                <div className='footer-col-i'>
                  <Link to='/courses'>Дополнительно</Link>
                </div>
              </div>
              <div className='footer-col'>
                <div className='footer-col-title'>События</div>
                <div className='footer-col-i'>
                  <Link to='/events'>Все события</Link>
                </div>
              </div>

              <div className='footer-col'>
                <div className='footer-col-title'>Контакты</div>
                <div className='footer-col-contact'>
                  <span uk-icon='location'></span>
                  <a target='_blank' href='https://go.2gis.com/2qc87m'>
                    г.Алматы, <br /> ул.Достык 162 к5
                  </a>
                </div>
                <div className='footer-col-contact'>
                  <span uk-icon='receiver'></span>
                  <div className='footer-col-contact-i'>
                    <a target='_blank' href='tel:87064003644'>
                      +7 706 400 36 44
                    </a>
                    <a target='_blank' href='tel:87071108232'>
                      +7 707 110 82 32
                    </a>
                  </div>
                </div>
                <div className='footer-col-contact'>
                  <span uk-icon='mail'></span>
                  <a target='_blank' href='mailto:contact@morrison.kz'>
                    contact@morrison.kz
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default Footer;
