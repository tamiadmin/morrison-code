import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import formatToPhone from '../../phone';
import './landing.scss';
import Form from '../../components/form';

const mapData = {
  center: [43.2367614, 76.9130467],
  zoom: 12,
};

const coordinates = [[43.2367614, 76.9130467]];

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeCourse: 'Все курсы',
      events: [],
      mounth: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь',
      ],
      form: {
        name: '',
        phone: '+7',
        email: '',
      },
      err: {
        name: false,
        phone: false,
        email: false,
      },
      send: false,
    };

    this.submitAll = this.submitAll.bind(this);
    this.changePhone = this.changePhone.bind(this);
    this.changeForm = this.changeForm.bind(this);
  }

  componentDidMount() {
    axios
      .get(`/api/events`)
      .then(response => {
        this.setState({
          events: response.data,
        });
      })
      .catch(err => {});
  }

  submitAll() {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (
      this.state.form.phone.length > 5 &&
      this.state.form.name.length > 1 &&
      re.test(this.state.form.email)
    ) {
      axios
        .post(`/api/send/`, this.state.form)
        .then(response => {
          this.setState({
            ...this.state,
            send: true,
            form: {
              name: '',
              phone: '+7',
              email: '',
            },
          });
        })
        .catch(err => {});
    } else {
      var err = this.state.err;

      if (this.state.form.name.length < 2) {
        err.name = true;
      }

      if (this.state.form.phone.length < 3) {
        err.phone = true;
      }

      if (!re.test(this.state.form.email)) {
        err.email = true;
      }

      this.setState({ err: err });
    }
  }

  changeForm(t, e) {
    this.setState({
      form: {
        ...this.state.form,
        [t]: e.target.value,
      },
      err: {
        ...this.state.err,
        [t]: false,
      },
    });
  }

  changePhone(e) {
    const value = formatToPhone(e);

    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        phone: value,
      },
      err: {
        ...this.state.err,
        phone: false,
      },
    });
  }

  changeModal(type) {
    this.setState({ showmodal: type });
  }

  render() {
    const form = this.state.form;

    return (
      <Fragment>
        <div className='main'>
          <div className='main-welcome'>
            <div className='main-welcome-bg'>
              <div className='container'>
                <div className='main-welcome-inner'>
                  <div className='main-welcome-content'>
                    <div className='main-welcome-title'>
                      Школа программирования Morrison
                    </div>
                    <div className='main-welcome-text'>
                      Освой востребоваенную it-проффесию c нуля от 24950тг
                    </div>
                    <div className='main-welcome-button'>
                      <button
                        onClick={() => this.props.history.push('/courses')}
                      >
                        Записаться на курс
                      </button>
                    </div>
                  </div>
                  <div className='main-welcome-form'>
                    <div className='main-welcome-form-title'>
                      Бесплатная консультация
                    </div>
                    <Form />
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end welcome */}
          <div className='main-about'>
            <div className='container'>
              <div className='main-about-inner'>
                <div className='main-about-left'>
                  <div className='main-about-title'>Morrison Code</div>
                  <div className='main-about-text'>
                    Наши программы обучения актуальные и практичные. Мы
                    гарантируем ученикам максимальный результат за минимальный
                    срок.
                  </div>
                </div>
                <div className='main-about-items'>
                  <div className='main-about-items-i'>
                    <div className='main-about-items-i-img'>
                      <img src='/images/icons/practice.svg' alt='' />
                    </div>
                    <div className='main-about-items-i-content'>
                      <div className='main-about-items-i-title'>
                        Меньше теории – больше практики
                      </div>
                      <div className='main-about-items-i-text'>
                        На наших занятиях нет сухой теории. Здесь – только то,
                        что вы сможете использовать в работе. Все задания и
                        примеры – из реальной практики.
                      </div>
                    </div>
                  </div>
                  <div className='main-about-items-i'>
                    <div className='main-about-items-i-img'>
                      <img src='/images/icons/simple.svg' alt='' />
                    </div>
                    <div className='main-about-items-i-content'>
                      <div className='main-about-items-i-title'>
                        Просто о сложном
                      </div>
                      <div className='main-about-items-i-text'>
                        Мы любим говорить о сложных вещах понятными словами.
                        Процесс обучения интенсивный, но легкий и интересный.
                      </div>
                    </div>
                  </div>
                  <div className='main-about-items-i'>
                    <div className='main-about-items-i-img'>
                      <img src='/images/icons/leactor.svg' alt='' />
                    </div>
                    <div className='main-about-items-i-content'>
                      <div className='main-about-items-i-title'>
                        Опытные лекторы
                      </div>
                      <div className='main-about-items-i-text'>
                        У нас преподают лекторы с многолетним стажем работы. Они
                        делятся с учениками самой полезной информацией и дают
                        только дельные советы.
                      </div>
                    </div>
                  </div>
                  <div className='main-about-items-i'>
                    <div className='main-about-items-i-img'>
                      <img src='/images/icons/help.svg' alt='' />
                    </div>
                    <div className='main-about-items-i-content'>
                      <div className='main-about-items-i-title'>
                        Помощь на всех этапах
                      </div>
                      <div className='main-about-items-i-text'>
                        Мы консультируем учеников в процессе и после обучения.
                        Лучшим из лучших помогаем найти работу. А самые
                        талантливые станут частью нашей команды.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end about */}
          <div className='main-partners'>
            <div className='container'>
              <div className='main-partners-inner'>
                <div className='main-partners-content'>
                  Мы помогаем нашим выпускникам{' '}
                  <span>получить стажировку и работу </span> в ведущих компаниях
                </div>
                <div className='main-partners-items'>
                  {[...Array(5)].map((i, k) => {
                    return (
                      <img
                        key={k}
                        src={`/images/partners/${k + 1}.png`}
                        alt=''
                      />
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
          <div className='main-courses'>
            <div className='container'>
              <div className='main-courses-inner'>
                <div className='main-courses-title'>
                  Выберите актуальные <br /> для вас курсы
                </div>
                <div className='main-courses-info'>
                  <div className='main-courses-img'>
                    <img src='/images/icons/up.svg' alt='' />
                  </div>
                  <div className='main-courses-text'>
                    Академия Morrison – это команда топовых специалистов в своей
                    отрасли. Мы постоянно пересматриваем и улучшаем нашу систему
                    обучения. Смягчаем сложные практические задания легкостью
                    объяснения. А требовательность преподавателей – юмором и
                    простотой общения. Никто не ценит ваше время больше, чем мы.
                  </div>
                  <div className='main-courses-button'>
                    <span onClick={() => this.props.history.push('/courses')}>
                      Все курсы
                    </span>
                  </div>
                </div>
                <div className='main-courses-content'>
                  <div className='main-courses-content-title'>
                    {[
                      'Все курсы',
                      // 'Программирование',
                      // 'Языковая школа',
                      // 'Дополнительно',
                    ].map((i, k) => {
                      return this.state.activeCourse === i ? (
                        <span className='main-courses-content-title-active'>
                          {i}
                        </span>
                      ) : (
                        <span>{i}</span>
                      );
                    })}
                  </div>
                  <div className='main-courses-items'>
                    <Link
                      to='/page/react'
                      className='main-courses-items-i'
                      style={{
                        backgroundImage: 'url(/images/pages/react.png)',
                      }}
                    >
                      <div className='main-courses-items-i-bg'>
                        <div className='main-courses-items-i-type'>
                          <div className='main-courses-items-i-badge'>
                            <span>Продвинутый</span>
                          </div>
                          <div className='main-courses-items-i-name'>
                            Курсы по React
                          </div>
                        </div>
                        <div className='main-courses-items-i-start'>
                          Для амбициозных верстальщиков и Junior
                          Frontend-разработчиков, желающих расти
                        </div>
                      </div>
                    </Link>
                    <Link
                      to='/page/web'
                      className='main-courses-items-i'
                      style={{
                        backgroundImage: 'url(/images/pages/web.png)',
                      }}
                    >
                      <div className='main-courses-items-i-bg'>
                        <div className='main-courses-items-i-type'>
                          <div className='main-courses-items-i-badge'>
                            <span>Для начинающих</span>
                          </div>
                          <div className='main-courses-items-i-name'>
                            Web-разработка с нуля
                          </div>
                        </div>
                        <div className='main-courses-items-i-start'>
                          Прокачайся с нуля до junior front-end developer всего
                          за 2 месяцев
                        </div>
                      </div>
                    </Link>
                    <Link
                      to='/page/python'
                      className='main-courses-items-i'
                      style={{
                        backgroundImage: 'url(/images/pages/python.png)',
                      }}
                    >
                      <div className='main-courses-items-i-bg'>
                        <div className='main-courses-items-i-type'>
                          <div className='main-courses-items-i-badge'>
                            <span>Для начинающих</span>
                          </div>
                          <div className='main-courses-items-i-name'>
                            Python-разработчик
                          </div>
                        </div>
                        <div className='main-courses-items-i-start'>
                          Научитесь с нуля программировать на Python
                        </div>
                      </div>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end courses */}
          {/* subscribe */}
          <div className='main-sub'>
            <div className='container'>
              <div className='main-sub-inner'>
                <div className='main-sub-content'>
                  <div className='main-sub-title'>
                    Хочешь получать лучшие <br />
                    статьи по программированию ?
                  </div>
                  <div className='main-sub-text'>Подпишись на рассылку</div>
                </div>
                <div className='main-sub-send'>
                  <input type='text' placeholder='Email' />
                  <div className='main-sub-button'>Подписаться</div>
                </div>
              </div>
            </div>
          </div>
          {/* end subscribe */}
          {/* materials */}
          <div className='main-mater'>
            <div className='container'>
              <div className='main-mater-inner'>
                <div className='main-mater-content'>
                  <div className='main-mater-title'>
                    Бесплатные материалы <br /> которые помогут тебе стать
                    программистом
                  </div>
                  <div className='main-mater-text'>
                    Получите бесплатные видео и статьи, которые <br /> сделают
                    тебя ближе к работе мечты
                  </div>
                  <div className='main-mater-button'>
                    <a
                      target='_blank'
                      href='https://ravesli.com/70-besplatnyh-resursov-dlya-izucheniya-programmirovaniya/'
                    >
                      Скачать бесплатно
                    </a>
                  </div>
                </div>
                <div className='main-mater-img'>
                  <img src='/images/back/web.svg' alt='' />
                </div>
              </div>
            </div>
          </div>
          {/* end materials */}
          {/* events */}
          <div className='main-events'>
            <div className='container'>
              <div className='main-container-inner'>
                <div className='main-events-title'>
                  <span>Наши события</span>
                  <Link to='/events'>Посмотреть все события</Link>
                </div>
                <div className='main-events-items'>
                  {this.state.events.map((i, k) => {
                    return (
                      <Link
                        to={'/events/' + i._id}
                        key={k}
                        className='events-items-i'
                      >
                        <div className='events-items-i-type'>
                          <div className='events-items-i-badge'>
                            <span>Пробный урок</span>
                          </div>
                          <div className='events-items-i-name'>{i.name}</div>
                        </div>
                        <div className='events-items-i-start'>
                          {i.date.substring(8, 10)}{' '}
                          {
                            this.state.mounth[
                              parseInt(i.date.substring(5, 7)) - 1
                            ]
                          }{' '}
                          {i.date.substring(0, 4)}
                        </div>
                      </Link>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
          {/* end events */}
          {/* questions */}
          <div className='main-q'>
            <div className='container'>
              <div className='main-q-inner'>
                <div className='main-q-content'>
                  <div className='main-q-title'>Остались вопросы?</div>
                  <div className='main-q-text'>Пишите или звоните</div>
                </div>
                <div className='main-q-items'>
                  <div className='main-q-i'>
                    <div className='main-q-i-name'>Телефон:</div>
                    <div className='main-q-i-number'>
                      <a href='tel:87064003644'>+7 706 400-36-44</a>
                    </div>
                    <div className='main-q-i-number'>
                      <a href='tel:87064126705'>+7 706 412-67-05</a>
                    </div>
                  </div>
                  <div className='main-q-i'>
                    <div className='main-q-i-name'>E-mail:</div>
                    <div className='main-q-i-number'>
                      <a href='mailto:code@morrison.kz' target='_blank'>
                        code@morrison.kz
                      </a>
                    </div>
                  </div>
                  <div className='main-q-i'>
                    <div className='main-q-i-name'>Телеграмм:</div>
                    <div className='main-q-i-number'>
                      <a
                        target='_blank'
                        href='https://t.me/joinchat/AAAAAEJbiJqU4bnmEEy5gA'
                      >
                        @morrisoncode
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end questions */}
        </div>
      </Fragment>
    );
  }
}

export default Main;
