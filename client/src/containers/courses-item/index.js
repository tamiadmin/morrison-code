import React, { Fragment, Component } from 'react';

import $ from 'jquery';

import './course.scss';
import axios from 'axios';

class CoursesItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      course: null,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {};
  }

  componentDidMount() {
    axios
      .get(`/api/course`, {
        id: this.props.match.params.id,
      })
      .then(response => {
        this.setState({
          course: response.data,
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidUpdate(prevProps) {}

  render() {
    return (
      <Fragment>
        <div className='course'>
          n
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default (CoursesItem);
