import React, { Fragment, Component } from 'react';
import { Link } from 'react-router-dom';
import Modal from 'react-animated-modal';

import './header.scss';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showmenu: false,
    };

    this.showMenu = this.showMenu.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {};
  }

  componentDidMount() {}

  componentDidUpdate(prevProps) {}

  showMenu() {
    this.setState({ showmenu: !this.state.showmenu });
  }

  render() {
    return (
      <Fragment>
        <div className='header'>
          <div className='container'>
            <div className='header-inner'>
              <Link to='/' className='header-logo'>
                <img src='/images/logo.png' alt='' />
              </Link>
              <div className='header-menu'>
                <Link to={'/'} className='header-menu-i'>
                  Главная
                </Link>
                <Link to={'/courses'} className='header-menu-i'>
                  Курсы
                </Link>
                <Link to={'/events'} className='header-menu-i'>
                  События
                </Link>
                <Link to={'/about'} className='header-menu-i'>
                  Об академии
                </Link>
                <Link to={'/contact'} className='header-menu-i'>
                  Контакты
                </Link>
              </div>
              <div className='header-contact'>Записаться на урок</div>
              <div className='header-burger' onClick={() => this.showMenu()}>
                <span uk-icon='icon: menu;ratio: 1.5'></span>
              </div>
              <Modal
                visible={this.state.showmenu}
                closemodal={() => this.setState({ showmenu: false })}
                type='fadeInLeft'
              >
                <div className='header-modal'>
                  <div className='header-modal-inner'>
                    <div className='header-modal-top'>
                      <div className='header-modal-top-title'>Меню</div>
                      <div className='header-modal-top-close'>
                        <span
                          onClick={() => this.showMenu()}
                          uk-icon='icon: close;ratio: 1.5'
                        ></span>
                      </div>
                    </div>
                    <Link
                      to={'/'}
                      onClick={this.showMenu}
                      className='header-modal-i'
                    >
                      Главная
                    </Link>
                    <Link
                      to={'/courses'}
                      onClick={this.showMenu}
                      className='header-modal-i'
                    >
                      Курсы
                    </Link>
                    <Link
                      to={'/events'}
                      onClick={this.showMenu}
                      className='header-modal-i'
                    >
                      События
                    </Link>
                    <Link
                      to={'/about'}
                      onClick={this.showMenu}
                      className='header-modal-i'
                    >
                      Об академии
                    </Link>
                    <Link
                      to={'/contact'}
                      onClick={this.showMenu}
                      className='header-modal-i'
                    >
                      Контакты
                    </Link>
                    <div className='header-modal-links'>
                      <a
                        href='https://www.instagram.com/morrisoncode_almaty/'
                        className='header-modal-link'
                      >
                        instagram
                      </a>
                      <a
                        href='https://api.whatsapp.com/send?phone=87064003644&text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5!%20!!%20%D0%A3%20%D0%BC%D0%B5%D0%BD%D1%8F%20%D0%B5%D1%81%D1%82%D1%8C%20%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81'
                        className='header-modal-link'
                      >
                        whatsapp
                      </a>
                    </div>
                  </div>
                </div>
              </Modal>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Header;
