import React, { Fragment, Component, useLayoutEffect } from 'react';

import { YMaps, Map, Placemark } from 'react-yandex-maps';

import axios from 'axios';

import './contact.scss';
import Form from '../../components/form';

const mapData = {
  center: [43.23415600458706, 76.96033463610858],
  zoom: 17,
};

const coordinates = [[43.23415600458706,76.96033463610858]];

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.sendMail = this.sendMail.bind(this);
  }

  componentDidMount() {}

  sendMail() {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (
      this.state.msg.length > 8 &&
      this.state.name.length > 1 &&
      re.test(this.state.mail)
    ) {
      axios
        .post(`/sendmail/`, {
          name: this.state.name,
          mail: this.state.mail,
          msg: this.state.msg,
        })
        .then(response => {
          this.setState({ showmodal: true, name: '', mail: '', msg: '' });
        })
        .catch(err => {});
    } else {
      var err = this.state.errUser;

      if (this.state.name.length < 2) {
        err.name = true;
      }

      if (this.state.msg.length < 9) {
        err.msg = true;
      }

      if (!re.test(this.state.mail)) {
        err.email = true;
      }

      this.setState({ errUser: err });
    }
  }

  changeModal(type) {
    this.setState({ showmodal: type });
  }

  render() {
    return (
      <Fragment>
        <div className='contact'>
          {/* top */}
          <div className='contact-top'>
            <div className='container'>
              <div className='contact-top-title'>КОНТАКТЫ</div>
            </div>
          </div>
          {/* end top */}
          {/* info */}
          <div className='contact-info'>
            <div className='container'>
              <div className='contact-info-inner'>
                <div className='contact-info-left'>
                  <div className='contact-info-content'>
                    <div className='contact-info-text'>
                      г.Алматы, ул. Достык 162 к5
                    </div>
                    <div className='contact-info-i'>
                      <div className='contact-info-i-badge'>
                        Номер телефона:
                      </div>
                      <div className='contact-info-i-content'>
                        <a href='tel:+77064003644' target='_blank'>
                          +7 706 400 36 44
                        </a>
                      </div>
                    </div>
                    <div className='contact-info-i'>
                      <div className='contact-info-i-badge'>
                        Сотрудничество:
                      </div>
                      <div className='contact-info-i-content'>
                        <a href='tel:+77071108232' target='_blank'>
                          +7 707 110 82 32
                        </a>
                      </div>
                    </div>
                    <div className='contact-info-i'>
                      <div className='contact-info-i-badge'>Почта:</div>
                      <div className='contact-info-i-content'>
                        <a href='mailto:contact@morrison.kz' target='_blank'>
                          contact@morrison.kz
                        </a>
                      </div>
                    </div>
                  </div>

                  <div className='contact-info-social'>
                    <div className='contact-info-social-title'>
                      Наши соц сети:
                    </div>
                    <div className='contact-info-social-i'>
                      <a
                        href='https://www.instagram.com/morrisoncode_almaty/'
                        class='uk-icon-button  uk-margin-small-right'
                        uk-icon='icon: instagram ; ratio: 3.5'
                      ></a>
                      <a
                        href='https://api.whatsapp.com/send?phone=87064003644&text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5!%20!!%20%D0%A3%20%D0%BC%D0%B5%D0%BD%D1%8F%20%D0%B5%D1%81%D1%82%D1%8C%20%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81'
                        class='uk-icon-button'
                        uk-icon='icon: whatsapp ; ratio: 3.5'
                      ></a>
                    </div>
                  </div>
                </div>
                <div className='contact-info-right'>
                  <div className='contact-info-right-title'>
                    Связаться с нами
                  </div>
                  <Form />
                </div>
              </div>
            </div>
          </div>
          {/* end info */}
          {/* map */}
          <div className='contact-map'>
            <YMaps>
              <Map defaultState={mapData} width='100%' height='100%'>
                <Placemark geometry={coordinates[0]} />
              </Map>
            </YMaps>
          </div>
          {/* end map */}
          {/* sub */}
          {/* <div className='main-sub'>
            <div className='container'>
              <div className='main-sub-inner'>
                <div className='main-sub-content'>
                  <div className='main-sub-title'>
                    Хочешь получать лучшие <br />
                    статьи от WebDes Guru ?
                  </div>
                  <div className='main-sub-text'>Подпишись на рассылку</div>
                </div>
                <div className='main-sub-send'>
                  <input type='text' placeholder='Email' />
                  <div className='main-sub-button'>Подписаться</div>
                </div>
              </div>
            </div>
          </div> */}
          {/* end sub */}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default Contact;
