import React, { Fragment, Component, useLayoutEffect } from 'react';

import './about.scss';

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeCourse: 'Все курсы',
    };
  }

  componentDidMount() {}
  changeModal(type) {
    this.setState({ showmodal: type });
  }

  render() {
    return (
      <Fragment>
        <div className='about'>
          <div className='about-welcome'>
            <div className='container'>
              <div className='about-welcome-inner'>
                <div className='about-welcome-title'>
                  <span>MORRISON CODE</span> путеводители в мир IT
                </div>
                <div className='about-welcome-text'>
                  Мы обучаем востребованным интернет-профессиям, наши
                  образовательные курсы меняют жизни тысяч людей к лучшему
                </div>
                <div className='about-welcome-button'>
                  <span onClick={() => this.props.history.push('/courses')}>
                    Выбрать курс
                  </span>
                </div>
              </div>
            </div>
          </div>
          {/* end welcome */}
          <div className='about-about'>
            <div className='container'>
              <div className='about-about-inner'>
                <div className='about-about-items'>
                  <div className='about-about-items-i'>
                    <div className='about-about-items-i-img'>
                      <img src='' alt='' />
                    </div>
                    <div className='about-about-items-i-content'>
                      <div className='about-about-items-i-title'>
                        Меньше теории – больше практики
                      </div>
                      <div className='about-about-items-i-text'>
                        На наших занятиях нет сухой теории. Здесь – только то,
                        что вы сможете использовать в работе. Все задания и
                        примеры – из реальной практики.
                      </div>
                    </div>
                  </div>
                  <div className='about-about-items-i'>
                    <div className='about-about-items-i-img'>
                      <img src='' alt='' />
                    </div>
                    <div className='about-about-items-i-content'>
                      <div className='about-about-items-i-title'>
                        Просто о сложном
                      </div>
                      <div className='about-about-items-i-text'>
                        Мы любим говорить о сложных вещах понятными словами.
                        Процесс обучения интенсивный, но легкий и интересный.
                      </div>
                    </div>
                  </div>
                  <div className='about-about-items-i'>
                    <div className='about-about-items-i-img'>
                      <img src='' alt='' />
                    </div>
                    <div className='about-about-items-i-content'>
                      <div className='about-about-items-i-title'>
                        Опытные лекторы
                      </div>
                      <div className='about-about-items-i-text'>
                        У нас преподают лекторы с многолетним стажем работы. Они
                        делятся с учениками самой полезной информацией и дают
                        только дельные советы.
                      </div>
                    </div>
                  </div>
                  <div className='about-about-items-i'>
                    <div className='about-about-items-i-img'>
                      <img src='' alt='' />
                    </div>
                    <div className='about-about-items-i-content'>
                      <div className='about-about-items-i-title'>
                        Помощь на всех этапах
                      </div>
                      <div className='about-about-items-i-text'>
                        Мы консультируем учеников в процессе и после обучения.
                        Лучшим из лучших помогаем найти работу. А самые
                        талантливые станут частью нашей команды.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end about */}
          {/* founder */}
          <div className='about-founder'>
            <div className='container'>
              <div className='about-founder-inner'>
                <div className='about-founder-content'>
                  <div className='about-founder-title'>Добро пожаловать!</div>
                  <div className='about-founder-text'>
                    Я верю, что учеба может быть продуктивной, но при этом
                    увлекательной. Что знания должны быть практичными. А любой
                    профессией можно овладеть в сжатые сроки. Именно поэтому я
                    основал Академию Morrison. Мы исповедуем принцип
                    доступности. Доступным языком о самом сложном.А главное –
                    быстрый, но эффективный формат обучения. Мечта гораздо
                    ближе, чем вы думаете!
                  </div>
                  <div className='about-founder-name'>Мунарбеков Тахир</div>
                  <div className='about-founder-job'>
                    Генеральный директор Morrison Code
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end founder */}
          {/* team */}
          <div className='about-team'>
            <div className='container'>
              <div className='about-team-inner'>
                <div className='about-team-title'>Наша команда</div>
                <div className='about-team-items'>
                  <div className='about-team-items-i'>
                    <div className='about-team-items-i-cover'>
                      <div
                        className='about-team-items-i-img'
                        style={{
                          backgroundImage: 'url(/images/team/takhir.jpg)',
                        }}
                      ></div>
                    </div>
                    <div className='about-team-items-i-type'>
                      Куратор курсов
                    </div>
                    <div className='about-team-items-i-name'>
                      Мунарбеков Тахир
                    </div>
                    <div className='about-team-items-i-text'>
                      Каждый день мы стараемся сделать лучший контент для нашей
                      школы. Каждый курс мы проходим всей командой, для того
                      чтобы решить достоин он наших учеников или нет.
                    </div>
                  </div>
                  <div className='about-team-items-i'>
                    <div className='about-team-items-i-cover'>
                      <div
                        className='about-team-items-i-img'
                        style={{
                          backgroundImage: 'url(/images/team/tair.jpg)',
                        }}
                      ></div>
                    </div>
                    <div className='about-team-items-i-type'>Маркетолог</div>
                    <div className='about-team-items-i-name'>
                      Турсонов Таиржан
                    </div>
                    <div className='about-team-items-i-text'>
                      Morrison Code – это постоянное развитие. Мы учим и учимся
                      сами. Следим за новинками, тестируем разные подходы,
                      прислушиваемся к критике. Стремимся к совершенству ради
                      вас и вместе с вами!
                    </div>
                  </div>
                  <div className='about-team-items-i'>
                    <div className='about-team-items-i-cover'>
                      <div
                        className='about-team-items-i-img'
                        style={{
                          backgroundImage: 'url(/images/team/stas.jpg)',
                        }}
                      ></div>
                    </div>
                    <div className='about-team-items-i-type'>
                      Куратор курсов
                    </div>
                    <div className='about-team-items-i-name'>
                      Югай Станислав
                    </div>
                    <div className='about-team-items-i-text'>
                      Я рад, что помогаю людям менять жизнь к лучшему. Освоить
                      за пару месяцев новую профессию. Выйти на достойный
                      заработок. Открыть для себя увлекательный мир
                      программирования. Самое ценное для меня – искренняя
                      благодарность студентов.
                    </div>
                  </div>
                  <div className='about-team-items-i'>
                    <div className='about-team-items-i-cover'>
                      <div
                        className='about-team-items-i-img'
                        style={{
                          backgroundImage: 'url(/images/team/almas.jpg)',
                        }}
                      ></div>
                    </div>
                    <div className='about-team-items-i-type'>
                      Куратор курсов
                    </div>
                    <div className='about-team-items-i-name'>Ибраев Алмас</div>
                    <div className='about-team-items-i-text'>
                      Вы можете достичь результата только в том случае, если
                      идете к цели вместе с опытным куратором и наставником.
                      Добро пожаловать к нам. Мы рады что вы с нами
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end team */}
          {/* partners */}
          <div className='about-partners'>
            <div className='container'>
              <div className='about-partners-inner'>
                <div className='about-partners-i'>
                  <div className='about-partners-i-title'>
                    Хочу стать <div>преподавателем</div>
                  </div>
                  <div className='about-partners-i-text'>
                    Станьте частью команды <br /> Академии Morrison
                  </div>
                  <div className='about-partners-i-button'>
                    <a
                      target='_blank'
                      href='https://telegram.me/takhirmunarbekov'
                    >
                      Написать сообщение
                    </a>
                  </div>
                </div>
                <div className='about-partners-i'>
                  <div className='about-partners-i-title'>
                    Хотите стать<div> партнером?</div>
                  </div>
                  <div className='about-partners-i-text'>
                    Напишите нам, чтобы узнать о всех вариантах партнерства
                  </div>
                  <div className='about-partners-i-button'>
                    <a
                      target='_blank'
                      href='https://telegram.me/takhirmunarbekov'
                    >
                      Написать сообщение
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end partners */}
          {/* select */}
          <div className='about-select'>
            <div className='container'>
              <div className='about-select-inner'>
                <div className='about-select-content'>
                  <div className='about-select-title'>
                    Выберите свою точку старта! <br /> Вы уже готовы пройти
                    обучение
                  </div>
                  <div className='about-select-text'>
                    У нас есть курсы как для новичков, так и для опытных
                    специалистов. Ознакомьтесь с описанием и выберите то, что
                    подходит именно вам. Есть сомнения? Свяжитесь с нами, и мы
                    поможем вам с выбором.
                  </div>
                  <div className='about-select-button'>
                    <button onClick={() => this.props.history.push('/courses')}>
                      Выбрать курс
                    </button>
                  </div>
                </div>
                <div className='about-select-img'>
                  <img src='/images/back/back-about-select.png' alt='' />
                </div>
              </div>
            </div>
          </div>
          {/* end select */}
          {/* questions */}
          <div className='about-q'>
            <div className='container'>
              <div className='about-q-inner'>
                <div className='about-q-content'>
                  <div className='about-q-title'>Остались вопросы?</div>
                  <div className='about-q-text'>Пишите или звоните</div>
                </div>
                <div className='about-q-items'>
                  <div className='about-q-i'>
                    <div className='about-q-i-name'>Телефон:</div>
                    <div className='about-q-i-number'>
                      <a href='tel:87064003644'>+7 706 400-36-44</a>
                    </div>
                    <div className='about-q-i-number'>
                      <a href='tel:87064126705'>+7 706 412-67-05</a>
                    </div>
                  </div>
                  <div className='about-q-i'>
                    <div className='about-q-i-name'>E-mail:</div>
                    <div className='about-q-i-number'>
                      <a href='mailto:code@morrison.kz' target='_blank'>
                        code@morrison.kz
                      </a>
                    </div>
                  </div>
                  <div className='about-q-i'>
                    <div className='about-q-i-name'>Телеграмм:</div>
                    <div className='about-q-i-number'>
                      <a
                        target='_blank'
                        href='https://t.me/joinchat/AAAAAEJbiJqU4bnmEEy5gA'
                      >
                        @morrisoncode
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* end questions */}
          {/* chat */}
          <div className='about-chat'>
            <div className='container'>
              <div className='about-chat-inner'>
                <div className='about-chat-title'>Чат для программистов</div>
                <div className='about-chat-text'>
                  Здесь вы можете задать любой вопрос по программированию и
                  получить
                  <br />
                  ответ нашего комьюнити
                </div>
                <div className='about-chat-button'>
                  <a
                    target='_blank'
                    href='https://t.me/joinchat/KLWGnBXZ1Yq1IaBSmfL-RQ'
                  >
                    Перейти в чат
                  </a>
                </div>
              </div>
            </div>
          </div>
          {/* end chat */}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default About;
