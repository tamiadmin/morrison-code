import React, { Fragment, Component } from 'react';
import { Link } from 'react-router-dom';

import './courses.scss';

class Courses extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeCourse: 'Все курсы',
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {};
  }

  componentDidMount() {}

  componentDidUpdate(prevProps) {}

  render() {
    return (
      <Fragment>
        <div className='courses'>
          <div className='courses-top'>
            <div className='container'>
              <div className='courses-top-inner'>
                <div className='courses-top-title'>НАШИ КУРСЫ</div>
                <div className='courses-top-text'>
                  Обучение в Академии Morrison – это насыщенные занятия, много
                  практики, помощь и поддержка преподавателей. У нас есть курсы
                  для любого уровня подготовки. Обучаем быстро, гарантируем
                  результат.
                </div>
              </div>
            </div>
          </div>
          {/* <div className='courses-type'>
            <div className='container'>
              <div className='courses-type-inner'>
                {[
                  'Все курсы',
                  'Программирование',
                  'Языковая школа',
                  'Дополнительно',
                ].map((i, k) => {
                  return this.state.activeCourse === i ? (
                    <span className='courses-type-inner-active'>{i}</span>
                  ) : (
                    <span>{i}</span>
                  );
                })}
              </div>
            </div>
          </div> */}
          <div className='courses-items'>
            <div className='container'>
              <div className='courses-items-inner'>
                <Link
                  to='/page/react'
                  className='main-courses-items-i'
                  style={{
                    backgroundImage: 'url(/images/pages/react.png)',
                  }}
                >
                  <div className='main-courses-items-i-bg'>
                    <div className='main-courses-items-i-type'>
                      <div className='main-courses-items-i-badge'>
                        <span>Продвинутый</span>
                      </div>
                      <div className='main-courses-items-i-name'>
                        Курсы по React
                      </div>
                    </div>
                    <div className='main-courses-items-i-start'>
                      Для амбициозных верстальщиков и Junior
                      Frontend-разработчиков, желающих расти
                    </div>
                  </div>
                </Link>
                <Link
                  to='/page/web'
                  className='main-courses-items-i'
                  style={{
                    backgroundImage: 'url(/images/pages/web.png)',
                  }}
                >
                  <div className='main-courses-items-i-bg'>
                    <div className='main-courses-items-i-type'>
                      <div className='main-courses-items-i-badge'>
                        <span>Для начинающих</span>
                      </div>
                      <div className='main-courses-items-i-name'>
                        Web-разработка с нуля
                      </div>
                    </div>
                    <div className='main-courses-items-i-start'>
                      Прокачайся с нуля до junior front-end developer всего за 2
                      месяцев
                    </div>
                  </div>
                </Link>
                <Link
                  to='/page/python'
                  className='main-courses-items-i'
                  style={{
                    backgroundImage: 'url(/images/pages/python.png)',
                  }}
                >
                  <div className='main-courses-items-i-bg'>
                    <div className='main-courses-items-i-type'>
                      <div className='main-courses-items-i-badge'>
                        <span>Для начинающих</span>
                      </div>
                      <div className='main-courses-items-i-name'>
                        Python-разработчик
                      </div>
                    </div>
                    <div className='main-courses-items-i-start'>
                      Научитесь с нуля программировать на Python
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default (Courses);
