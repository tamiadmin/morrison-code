import React, { Fragment, Component, useLayoutEffect } from 'react';

import axios from 'axios';
import formatToPhone from '../../phone';

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        name: '',
        phone: '+7',
        email: '',
      },
      err: {
        name: false,
        phone: false,
        email: false,
      },
      send: false,
    };

    this.submitAll = this.submitAll.bind(this);
    this.changePhone = this.changePhone.bind(this);
    this.changeForm = this.changeForm.bind(this);
  }

  componentDidMount() {}

  submitAll() {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (
      this.state.form.phone.length > 5 &&
      this.state.form.name.length > 1 &&
      re.test(this.state.form.email)
    ) {
      axios
        .post(`/api/send/`, this.state.form)
        .then(response => {
          this.setState({
            ...this.state,
            send: true,
            form: {
              name: '',
              phone: '+7',
              email: '',
            },
          });
        })
        .catch(err => {});
    } else {
      var err = this.state.err;

      if (this.state.form.name.length < 2) {
        err.name = true;
      }

      if (this.state.form.phone.length < 3) {
        err.phone = true;
      }

      if (!re.test(this.state.form.email)) {
        err.email = true;
      }

      this.setState({ err: err });
    }
  }

  changeForm(t, e) {
    this.setState({
      form: {
        ...this.state.form,
        [t]: e.target.value,
      },
      err: {
        ...this.state.err,
        [t]: false,
      },
    });
  }

  changePhone(e) {
    const value = formatToPhone(e);

    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        phone: value,
      },
      err: {
        ...this.state.err,
        phone: false,
      },
    });
  }

  render() {
    const form = this.state.form;
    return (
      <Fragment>
        <form action='' className='main-welcome-form-inner'>
          {this.state.err.name ? (
            <footer className='err'>Имя слишком короткое</footer>
          ) : null}
          <input
            value={form.name}
            onChange={e => this.changeForm('name', e)}
            type='text'
            placeholder='Ваше имя'
          />
          {this.state.err.phone ? (
            <footer className='err'>Неправельный номер телефона</footer>
          ) : null}
          <input
            value={form.phone}
            onChange={e => this.changePhone(e)}
            type='text'
            placeholder='Номер телефона'
          />
          {this.state.err.email ? (
            <footer className='err'>Неправельно введена почта</footer>
          ) : null}
          <input
            value={form.email}
            onChange={e => this.changeForm('email', e)}
            type='text'
            placeholder='Почта'
          />
          {this.state.send ? (
            <div
              className={
                this.props.button === 'white'
                  ? 'main-welcome-form-button'
                  : 'main-welcome-form-white'
              }
            >
              Спасибо!
            </div>
          ) : (
            <div
              className={
                !this.props.button
                  ? 'main-welcome-form-button'
                  : 'main-welcome-form-white'
              }
              onClick={() => this.submitAll()}
            >
              Оставить заявку
            </div>
          )}
        </form>
      </Fragment>
    );
  }
}

export default Form;
