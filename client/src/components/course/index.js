import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const Course = props => {
  return (
    <Link to={'/course'} className='courses-items-i'>
      <div className='courses-items-i-type'>
        <div className='courses-items-i-badge'>
          <span></span>
        </div>
        <div className='courses-items-i-name'>
          Курсы по Веб-программированию
        </div>
      </div>
      <div className='courses-items-i-start'>Старт 12 января 2020</div>
    </Link>
  );
};

export default Course;
