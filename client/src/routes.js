import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import Helmet from 'react-helmet';

import Header from '../src/containers/header';
import Landing from '../src/containers/landing';
import Courses from '../src/containers/courses';
import Events from '../src/containers/events';
import EventsItem from '../src/containers/events-item';
import About from '../src/containers/about';
import Contact from '../src/containers/contact';
import Footer from '../src/containers/footer';
import Admin from '../src/containers/admin';
import ReactPage from '../src/containers/pages/React';
import PyhonPage from '../src/containers/pages/Python';
import WebPage from '../src/containers/pages/Web';

const Routes = () => (
  <Fragment>
    <Helmet
      htmlAttributes={{ lang: 'en', amp: undefined }} // amp takes no value
      title='Morrison Code || Школа программирования в Алматы'
      titleTemplate='Morrison Code || Школа программирования в Алматы'
      defaultTitle='Morrison Code || Школа программирования в Алматы'
      link={[{ rel: 'icon', type: 'image/png', href: '/images/favicon.png' }]}
      meta={[
        {
          name: 'description',
          content:
            'Мы школа программирования которая поможет вам стать на шаг ближе к вашей мечте',
        },
        { property: 'fb:app_id', content: 'myfbid' },
        {
          property: 'og:title',
          content: 'Morrison Code || Школа программирования в Алматы',
        },
        { property: 'og:type', content: 'website' },
        { property: 'og:url', content: 'https://code.morrison.kz/' },
        { property: 'og:image', content: 'https://code.morrison.kz' },
        { property: 'icon', content: 'https://code.morrison.kz' },
        {
          property: 'og:description',
          content:
            'Мы школа программирования которая поможет вам стать на шаг ближе к вашей мечте',
        },
        {
          property: 'description',
          content:
            'Мы школа программирования которая поможет вам стать на шаг ближе к вашей мечте',
        },
        { property: 'og:site_name', content: 'code.morrison.kz' },
      ]}
    />
    <Route component={Header} />
    <Switch>
      <Route exact path='/' component={Landing} />
      <Route exact path='/courses' component={Courses} />
      <Route exact path='/events' component={Events} />
      <Route exact path='/events/:id' component={EventsItem} />
      <Route exact path='/about' component={About} />
      <Route exact path='/contact' component={Contact} />
      <Route exact path='/admin' component={Admin} />
      <Route exact path='/page/react' component={ReactPage} />
      <Route exact path='/page/web' component={WebPage} />
      <Route exact path='/page/python' component={PyhonPage} />
    </Switch>
    <Route component={Footer} />
  </Fragment>
);

export default Routes;
